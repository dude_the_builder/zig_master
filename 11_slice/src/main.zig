const std = @import("std");

// Slices let us handle sequences of items of any size.
fn display(items: []const u8) void {
    for (0..items.len) |i| std.debug.print("item {}: {}\n", .{ i, items[i] });
}

pub fn main() !void {
    const array = [_]u8{ 0, 1, 2, 3, 4, 0 };

    // Force runtime only bounds.
    var start: usize = 2;
    _ = &start;
    var len: usize = 3;
    _ = &len;

    // Create a slice.
    const slice = array[start..][0..len];
    std.debug.print("Type of slice: {}\n", .{@TypeOf(slice)});

    display(slice);

    // Create a sentinel terminated slice.
    const s_slice: [:0]const u8 = array[0 .. array.len - 1 :0];
    std.debug.print("Type of s_slice: {}\n", .{@TypeOf(s_slice)});
    std.debug.print("s_slice[s_slice.len]: {}\n", .{s_slice[s_slice.len]});

    display(s_slice);
}
