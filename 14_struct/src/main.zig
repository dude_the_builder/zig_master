const std = @import("std");

const Point = @import("Point.zig");

pub fn main() !void {
    const p1 = Point.new(1.0, 2.0);
    const p2 = Point.new(2.0, 3.0);
    const c = p1.add(p2);
    std.debug.print("c: {}\n", .{c});

    var d = Point.origin;
    Point.step = 2.0;
    d.increment();
    std.debug.print("d: {}\n", .{d});
}
