const std = @import("std");
const print = std.debug.print;

// Bare union.
const Number = union {
    float: f64,
    int: i8,
};

fn bare() void {
    print("size of Number: {}\n", .{@sizeOf(Number)});

    var num: Number = .{ .int = 42 };
    print("int: {}\n", .{num.int});

    num = .{ .float = 3.1415 };
    print("float: {}\n", .{num.float});
}

// Tagged union.
// const Tag = enum {
//     float,
//     int,
// };

const TaggedNumber = union(enum) {
    float: f64,
    int: u8,

    fn is(self: TaggedNumber, tag: std.meta.Tag(TaggedNumber)) bool {
        return self == tag;
    }
};

fn tagged() void {
    print("size of TaggedNumber: {}\n", .{@sizeOf(TaggedNumber)});

    var num: TaggedNumber = .{ .int = 42 };
    print("int: {}\n", .{num.int});

    num = .{ .float = 3.1415 };
    print("float: {}\n", .{num.float});

    // Coerce and compare to the tag type.
    if (num == .float) print("It's a float!\n", .{});

    num = .{ .int = 13 };

    // Switch on the tag type.
    switch (num) {
        .int => |i| print("int: {}\n", .{i}),
        .float => |f| print("float: {}\n", .{f}),
    }

    print("num is .int? {}\n", .{num.is(.int)});
}

pub fn main() !void {
    bare();
    tagged();
}
