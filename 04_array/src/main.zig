const std = @import("std");

// A big Thank You for your donations!
// * Jordan Frish
// * Damir Vandic
// * Bjorn Toft Madsen
// * Hakeem Orewole
// * Marcel Ochs

pub fn main() !void {
    // Array literal style.
    // const array = [_]u8{ 1, 2, 3 };

    // Tuple literal style
    // const array: [3]u8 = .{ 1, 2, 3 };

    // You can leave it undefined for now.
    var array: [3]u8 = undefined;
    // But be sure to initialize it before using it!
    // Element acces is via [indexing] operation.
    array[0] = 1;
    array[1] = 2;
    array[2] = 3;

    // Using destructuring syntax.
    array[0], array[1], array[2] = .{ 1, 2, 3 };

    // You can print the whole array with the `{any}` format
    // specifier.
    std.debug.print("{any}\n", .{array});

    // The `len` field has the length.
    std.debug.print("array.len: {}\n", .{array.len});
    // Indexing starts at 0, so you can't access
    // an index >= array.len.
    // std.debug.print("array[array.len]: {}\n", .{array[array.len]});

    // The other way around.
    const a, const b, const c = array;
    std.debug.print("a: {} b: {} c: {}\n", .{ a, b, c });

    // Multidimensional arrays are arrays of arrays.
    const grid3x3 = [_][3]u8{
        .{ 1, 2, 3 },
        .{ 4, 5, 6 },
        .{ 7, 8, 9 },
    };
    // Access as normal with indexing and sub-indexing.
    std.debug.print("grid3x3[2][1]: {}\n", .{grid3x3[2][1]});

    // Sentinel terminated array.
    const with_sentinel = [_:0]u8{ 1, 0, 0, 4 };
    // You can access the index at the array length.
    std.debug.print("sentinel: {}\n", .{with_sentinel[with_sentinel.len]});

    // String literals are actually pointers to sentinel
    // terminated arrays with sentinel 0. This is what's
    // known as a null terminated string in C.
    const str = "Hello";
    std.debug.print("type of str: {}\n", .{@TypeOf(str)});

    // But you can easily coerce this type into a slice of
    // bytes, which is what Zig calls a string.
    const bytes: []const u8 = str;
    std.debug.print("type of bytes: {}\n", .{@TypeOf(bytes)});

    // Arrays are copied by-value, so copies are new arrays.
    var copy = array;
    copy[2] = 42;
    std.debug.print("array: {any}\n", .{array});
    std.debug.print("copy: {any}\n", .{copy});
}
