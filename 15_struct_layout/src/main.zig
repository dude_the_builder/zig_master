const std = @import("std");

pub const S = struct {
    a: u8 = 1,
    b: u32 = 2,
    c: u8 = 3,
};

fn layout(s: *const S) void {
    // Info about struct type S.
    std.debug.print("Type:\t{}\n", .{S});
    std.debug.print("\tsize:\t{}\n", .{@sizeOf(S)});
    std.debug.print("\talign:\t{}\n", .{@alignOf(S)});
    std.debug.print("\n", .{});

    // Info about struct type S fields.
    const info = @typeInfo(S);

    inline for (info.Struct.fields) |field| {
        std.debug.print("Field:\t{s}\n", .{field.name});
        std.debug.print("\tsize:\t{}\n", .{@sizeOf(field.type)});
        std.debug.print("\toffset:\t{}\n", .{@offsetOf(S, field.name)});
        std.debug.print("\talign:\t{}\n", .{field.alignment});
        std.debug.print("\taddr:\t{*}\n", .{&@field(s, field.name)});
        std.debug.print("\n", .{});
    }
}

pub fn main() !void {
    const s = S{};
    layout(&s);
}
