const std = @import("std");

pub fn main() !void {
    // A mutable array.
    var array = [_]u8{ 0, 1, 2 };

    // A pointer to a mutable array.
    const ptr = &array;
    std.debug.print("\nType of ptr: {}\n", .{@TypeOf(ptr)});

    // Normal dereference syntax.
    ptr.*[0] = 9;
    std.debug.print("ptr.*[0]: {} , array[0]: {}\n", .{ ptr.*[0], array[0] });

    // Convenient indexing syntax.
    ptr[1] = 10;
    std.debug.print("ptr[1]: {} , array[1]: {}\n", .{ ptr[1], array[1] });

    // Single item pointer to array element.
    const item_ptr = &array[0];
    item_ptr.* = 99;
    std.debug.print("\nType of item_ptr: {}\n", .{@TypeOf(item_ptr)});
    std.debug.print("item_ptr.*: {}, ptr[0]: {} , array[0]: {}\n", .{
        item_ptr.*,
        ptr[0],
        array[0],
    });

    // The addresses are the same, but the types differ.
    std.debug.print("\n &array: {*} \n ptr: {*} \n item_ptr: {}\n", .{
        &array,
        ptr,
        item_ptr,
    });

    // A string literal is a pointers to a const sentinel-
    // terminated (0) array in the static memory block.
    const hello = "Héllo";
    std.debug.print("\nType of hello: {}\n", .{@TypeOf(hello)});
    std.debug.print("hello[0]: {0} {0c}\n", .{hello[0]});
    std.debug.print("hello.*[0]: {0} {0c}\n", .{hello.*[0]});
    std.debug.print("hello[1]: {0} {0c}\n", .{hello[1]});
    //hello[1] = 'z';
}
