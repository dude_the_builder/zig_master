# Zig Master Video Course Code Repository

This repository has the companion code shown throughout the 
[Zig Master video series](https://youtube.com/playlist?list=PLtB7CL7EG7pDKdSBA_AlNYrEsISOHBOQL&si=iiGU3hEjugpeYQH2).

La serie también está disponible [en Español](https://youtube.com/playlist?list=PLtB7CL7EG7pC960XOA5vymEyTfQ3M_QhL&si=JP0MyTkp2YW9uQAN).
