const std = @import("std");

pub fn main() !void {
    const x: u8 = 42;
    var y: u8 = 13;
    var z: u8 = 99;
    const a: u8 = 0;

    // const pointer to const value.
    const cpcx: *const u8 = &x;
    // cpcx.* += 1; // You can't do this!
    // cpcx = &y; // ...nor this!

    std.debug.print("cpcx: {}\tcpcx.*: {}\ttype: {}\n", .{
        cpcx,
        cpcx.*,
        @TypeOf(cpcx),
    });

    // const pointer to non-const value.
    const cpvy: *u8 = &y;
    cpvy.* += 1; // You can do this.
    // cpvy = &y; // ...but not this!

    std.debug.print("cpvy: {}\tcpvy.*: {}\ttype: {}\n", .{
        cpvy,
        cpvy.*,
        @TypeOf(cpvy),
    });

    // non-const pointer to const value.
    var vpcx: *const u8 = &x;
    // vpcx.* += 1; // You can't do this!

    std.debug.print("vpcx: {}\tvpcx.*: {}\ttype: {}\n", .{
        vpcx,
        vpcx.*,
        @TypeOf(vpcx),
    });

    vpcx = &a; // ...but you can do this.

    std.debug.print("vpcx: {}\tvpcx.*: {}\ttype: {}\n", .{
        vpcx,
        vpcx.*,
        @TypeOf(vpcx),
    });

    // non-const pointer to non-const value.
    var vpvy: *u8 = &y;
    vpvy.* += 1; // You can do this.

    std.debug.print("vpvy: {}\tvpvy.*: {}\ttype: {}\n", .{
        vpvy,
        vpvy.*,
        @TypeOf(vpvy),
    });

    vpvy = &z; // ...and this.

    std.debug.print("vpvy: {}\tvpvy.*: {}\ttype: {}\n", .{
        vpvy,
        vpvy.*,
        @TypeOf(vpvy),
    });
}
