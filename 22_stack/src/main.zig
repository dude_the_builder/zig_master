const std = @import("std");
const heap = std.heap;
const print = std.debug.print;

const Stack = @import("stack.zig").Stack;

pub fn main() !void {
    var gpa = heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    var stack = Stack(f32){ .allocator = allocator };
    defer stack.freeAndReset();

    for (0..10) |i| {
        try stack.push(@floatFromInt(i));
        print("{}\n", .{stack});
    }

    while (stack.pop()) |item| {
        print("{}\n", .{item});
        print("{}\n", .{stack});
    }

    stack.freeAndReset();

    for (0..20) |i| {
        try stack.push(@floatFromInt(i));
        print("{}\n", .{stack});
    }
}
