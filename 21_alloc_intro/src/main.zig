const std = @import("std");
const heap = std.heap;
const mem = std.mem;
const print = std.debug.print;
const unicode = std.unicode;

fn asBytes(allocator: mem.Allocator, code_points: []const u21) ![]u8 {
    var list = std.ArrayList(u8).init(allocator);
    defer list.deinit();

    var buf: [4]u8 = undefined;

    for (code_points) |cp| {
        const len = try unicode.utf8Encode(cp, &buf);
        try list.appendSlice(buf[0..len]);
    }

    return try list.toOwnedSlice();
}

fn asCodePointsAlloc(allocator: mem.Allocator, str: []const u8) ![]u21 {
    var list = std.ArrayList(u21).init(allocator);
    defer list.deinit();

    var view = try unicode.Utf8View.init(str);
    var iter = view.iterator();

    while (iter.nextCodepoint()) |cp| try list.append(cp);

    return try list.toOwnedSlice();
}

fn asCodePoints(str: []const u8, out: []u21) ![]u21 {
    var view = try unicode.Utf8View.init(str);
    var iter = view.iterator();

    var i: usize = 0;
    while (iter.nextCodepoint()) |cp| : (i += 1) out[i] = cp;

    return out[0..i];
}

fn fail() !void {
    return error.Fail;
}

pub fn main() !void {
    var gpa = heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    const ptr = try allocator.create(u8);
    defer allocator.destroy(ptr);
    ptr.* = 42;

    print("{*}\n", .{ptr});

    // try fail();

    const slice = try allocator.alloc(u8, 2);
    defer allocator.free(slice);

    slice[0] = 42;
    slice[1] = 43;

    print("{d}\n", .{slice});

    const code_points_in = [_]u21{ 'H', 'é', 'l', 'l', 'o', ' ', '🦎' };

    const str_out = try asBytes(allocator, &code_points_in);
    defer allocator.free(str_out);

    for (str_out) |b| print("{x} ", .{b});
    print("\n", .{});

    const code_points_out = try asCodePointsAlloc(allocator, str_out);
    defer allocator.free(code_points_out);

    for (code_points_out) |cp| print("{u} ", .{cp});
    print("\n", .{});

    const str_in = "Héllo 🦎";
    var buf: [str_in.len]u21 = undefined;

    const code_points_out_2 = try asCodePoints(str_in, &buf);

    for (code_points_out_2) |cp| print("{u} ", .{cp});
    print("\n", .{});
}
