#include <string.h>

// `strncpy` will only add terminating null bytes if
// `src`'s length is less than `len`.
char *copy(char * dst, const char *src, size_t len) {
	return strncpy(dst, src, len);
}
