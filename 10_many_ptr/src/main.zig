const std = @import("std");

// Make sure `src.len` is less than `len` or else you won't get
// a null byte at the end of the returned string!
extern fn copy(dst: [*]u8, src: [*]const u8, len: usize) [*:0]const u8;

pub fn main() !void {
    var array = [_]u8{ 0, 1, 2 };
    var ptr: [*]u8 = &array;
    std.debug.print("\nType of ptr: {}\n", .{@TypeOf(ptr)});

    // Indexing syntax.
    ptr[1] = 99;
    std.debug.print("ptr[1]: {}, array[1]: {}\n", .{ ptr[1], array[1] });

    // Pointer arithmetic (+ and -).
    ptr += 1;
    std.debug.print("ptr[1]: {}, array[1]: {}\n", .{ ptr[1], array[1] });
    ptr -= 1;
    std.debug.print("ptr[1]: {}, array[1]: {}\n", .{ ptr[1], array[1] });

    // Many item pointers are similar and interoperable with C
    // pointers. A sentinel (0) terminated many item pointer is
    // interoperable with a C null terminated string.
    const msg = "Hello";
    var buf: [10]u8 = undefined;
    const result = copy(&buf, msg, buf.len);
    std.debug.print("result: {s}\n", .{result});
}
